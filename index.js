//without the use of objects, our students from before would be organized as follows if we are to record additional information about them

//create student one
// let studentOneName = 'John';
// let studentOneEmail = 'john@mail.com';
// let studentOneGrades = [89, 84, 78, 88];

// //create student two
// let studentTwoName = 'Joe';
// let studentTwoEmail = 'joe@mail.com';
// let studentTwoGrades = [78, 82, 79, 85];

// //create student three
// let studentThreeName = 'Jane';
// let studentThreeEmail = 'jane@mail.com';
// let studentThreeGrades = [87, 89, 91, 93];

// //create student four
// let studentFourName = 'Jessie';
// let studentFourEmail = 'jessie@mail.com';
// let studentFourGrades = [91, 89, 92, 93];

// //actions that students may perform will be lumped together
// function login(email){
//     console.log(`${email} has logged in`);
// }

// function logout(email){
//     console.log(`${email} has logged out`);
// }

// function listGrades(grades){
//     grades.forEach(grade => {
//         console.log(grade);
//     })
// }

//This way of organizing employees is not well organized at all.
//This will become unmanageable when we add more employees or functions
//To remedy this, we will create objects

// Use an Object literal
// ENCAPSULATION

let studentOne = {
    name: 'John',
    email: 'john@mail.com',
    grades: [89, 84, 78, 88],
    login() {
        console.log(`${this.email} has logged in`);
    },
    logout() {
        console.log(`${this.email} has logged out`)
    },
    listGrades() {
        console.log(`${this.name}'s quarterly grade averages are: ${this.grades}`)
    },

    computeAve() {
        let sum = 0; 
        this.grades.forEach(grade => sum = sum + grade)
        return sum/4;

        // let sum = 0;
        // for (let num of this.grades){
        //     sum += num;
        // }
        // return sum/this.grade.length;
    },
    willPass() {
        // if (this.computeAve() >= 85) {
        //     return true
        // } else {
        //     return false
        // }

        return this.computeAve() >= 85 ? true : false

        // condition ? value if condition is true : value if condition is false
    },
    willPassWithHonors(){
        if (this.willPass() && this.computeAve() >= 90){
            return true
        } else if (this.computeAve() >= 85 && this.computeAve() < 90) {
            return false
        } else {
            return undefined
        }
        // return (this.willPass() && this.computeAve() >= 90) ? true : false
    }

}

// //create student two
// let studentTwoName = 'Joe';
// let studentTwoEmail = 'joe@mail.com';
// let studentTwoGrades = [78, 82, 79, 85];
let studentTwo = {
    name: 'Joe',
    email: 'joe@mail.com',
    grades: [78, 82, 79, 85],
    login() {
        console.log(`${this.email} has logged in`);
    },
    logout() {
        console.log(`${this.email} has logged out`)
    },
    listGrades() {
        console.log(`${this.name}'s quarterly grade averages are: ${this.grades}`)
    },
    computeAve() {
        let sum = 0; 
        this.grades.forEach(grade => sum = sum + grade)
        return sum/4;
    },
    willPass() {
        return this.computeAve() >= 85 ? true : false

    },
    willPassWithHonors(){
        if (this.willPass() && this.computeAve() >= 90){
            return true
        } else if (this.computeAve() >= 85 && this.computeAve() < 90) {
            return false
        } else {
            return undefined
        }
        // return (this.willPass() && this.computeAve() >= 90) ? true : false
    }
}

// //create student three
// let studentThreeName = 'Jane';
// let studentThreeEmail = 'jane@mail.com';
// let studentThreeGrades = [87, 89, 91, 93];
let studentThree = {
    name: 'Jane',
    email: 'jane@mail.com',
    grades: [87, 89, 91, 93],
    login() {
        console.log(`${this.email} has logged in`);
    },
    logout() {
        console.log(`${this.email} has logged out`)
    },
    listGrades() {
        console.log(`${this.name}'s quarterly grade averages are: ${this.grades}`)
    },
    computeAve() {
        let sum = 0; 
        this.grades.forEach(grade => sum = sum + grade)
        return sum/4;
    },
    willPass() {
        return this.computeAve() >= 85 ? true : false

    },
    willPassWithHonors(){
        if (this.willPass() && this.computeAve() >= 90){
            return true
        } else if (this.computeAve() >= 85 && this.computeAve() < 90) {
            return false
        } else {
            return undefined
        }
        // return (this.willPass() && this.computeAve() >= 90) ? true : false
    }
}

// //create student four
// let studentFourName = 'Jessie';
// let studentFourEmail = 'jessie@mail.com';
// let studentFourGrades = [91, 89, 92, 93];
let studentFour = {
    name: 'Jessie',
    email: 'jessie@mail.com',
    grades: [91, 89, 92, 93],
    login() {
        console.log(`${this.email} has logged in`);
    },
    logout() {
        console.log(`${this.email} has logged out`)
    },
    listGrades() {
        console.log(`${this.name}'s quarterly grade averages are: ${this.grades}`)
    },
    computeAve() {
        let sum = 0; 
        this.grades.forEach(grade => sum = sum + grade)
        return sum/4;
    },
    willPass() {
        return this.computeAve() >= 85 ? true : false

    },
    willPassWithHonors(){
        if (this.willPass() && this.computeAve() >= 90){
            return true
        } else if (this.computeAve() >= 85 && this.computeAve() < 90) {
            return false
        } else {
            return undefined
        }
        // return (this.willPass() && this.computeAve() >= 90) ? true : false
    }
}

// Class of 1A

let classOf1A = {
    students: [
        studentOne,
        studentTwo,
        studentThree,
        studentFour,
    ],
    countHonorStudents(){
        let result = 0;
        this.students.forEach(student =>{
        if (student.willPassWithHonors()){
            result++;
        }
        })
        return result;
    },
    honorsPercentage(){
        return(this.countHonorStudents() / this.students.length) * 100;
    },
    retrieveHonorStudentsInfo(){
        let honorStudents = [];
        this.students.forEach(student => {
            if(student.willPassWithHonors()) honorStudents.push({
                email: student.email,
                aveGrade: student.computeAve()
            })
        })
        return honorStudents;
    }
}


/*
QUIZ
1. Spagetti code
2. {}
3. Encapsulation
4. studentOne.enroll()
5. True
6. let obj = {key1: value1, key2: value2};
7. True
8. True
9. True
10. True
*/